FROM gradle:alpine AS build
COPY --chown=gradle:gradle docker /watchlater
WORKDIR /watchlater
RUN gradle build --no-daemon
FROM openjdk:11-slim
COPY --from=build /build/libs/watchlater.jar watchlater.jar
CMD ["java", "-jar", "watchlater.jar"]
EXPOSE 8080