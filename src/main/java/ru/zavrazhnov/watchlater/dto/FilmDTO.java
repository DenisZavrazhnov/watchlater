package ru.zavrazhnov.watchlater.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.zavrazhnov.watchlater.models.Film;

@Builder
@Data
@ApiModel(description = "DTO for films")
@AllArgsConstructor
@NoArgsConstructor
public class FilmDTO {

    @ApiModelProperty(notes = "Film id")
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty(notes = "Film title")
    @JsonProperty("title")
    private String title;

    @ApiModelProperty(notes = "Film timestamp, it can be Null")
    @JsonProperty("timestamp")
    private Long timestamp;

    public Film fromDtoToFilm(FilmDTO filmDTO) {
        return Film.builder()
                .title(filmDTO.getTitle())
                .timestamp(filmDTO.getTimestamp())
                .build();
    }

    public static FilmDTO fromFilm(Film film) {
        return FilmDTO.builder()
                .id(film.getId())
                .title(film.getTitle())
                .timestamp(film.getTimestamp())
                .build();
    }
}
