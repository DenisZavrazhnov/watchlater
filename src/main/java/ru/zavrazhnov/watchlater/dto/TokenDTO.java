package ru.zavrazhnov.watchlater.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@ApiModel(description = "DTO for JWT")
@NoArgsConstructor
public class TokenDTO {

    @ApiModelProperty(notes = "JWT")
    @JsonProperty("token")
    private String token;
}
