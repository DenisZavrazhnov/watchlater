package ru.zavrazhnov.watchlater.dto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
@ApiModel(description = "DTO for User")
public class UserDTO {

    @ApiModelProperty(notes = "User id")
    private Long id;

    @ApiModelProperty(notes = "User login\name")
    private String name;

    @ApiModelProperty(notes = "User id")
    private String password;

    @ApiModelProperty(notes = "User film list")
    private List<FilmDTO> films;

    @ApiModelProperty(notes = "Profile photo id")
    private String photoId;

}
