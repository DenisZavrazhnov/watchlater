package ru.zavrazhnov.watchlater.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ApiModel(description = "DTO to create new User, and getting JWT")
@AllArgsConstructor
@ToString
public class SignInDTO {

    @ApiModelProperty(notes = "Login")
    private String name;

    @ApiModelProperty(notes = "Password")
    private String password;
}
