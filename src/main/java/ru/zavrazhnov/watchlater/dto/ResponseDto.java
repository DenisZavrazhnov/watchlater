package ru.zavrazhnov.watchlater.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class ResponseDto {
    private String responseMessage;
    private long timestamp;

    public ResponseDto(String responseMessage) {
        this.responseMessage = responseMessage;
        this.timestamp = Instant.now().getEpochSecond();
    }
}
