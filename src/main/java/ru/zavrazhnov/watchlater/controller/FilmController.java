package ru.zavrazhnov.watchlater.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.zavrazhnov.watchlater.config.jwt.UserDetailsImpl;
import ru.zavrazhnov.watchlater.dto.FilmDTO;
import ru.zavrazhnov.watchlater.dto.ResponseDto;
import ru.zavrazhnov.watchlater.service.FilmService;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/film")
@RequiredArgsConstructor
public class FilmController {
    private final FilmService filmService;

    @ApiOperation("Getting a list of user's movies")
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping()
    public ResponseEntity<List<FilmDTO>> getFilmsByUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        return ResponseEntity.ok(filmService.getAllFilms(userDetails.getUserId()));
    }

    @ApiOperation("Adding movies to the user's list")
    @PreAuthorize("hasAuthority('USER')")
    @PostMapping()
    public ResponseEntity<ResponseDto> addFilms(
            @Validated(FilmDTO.class)
            @RequestBody FilmDTO filmDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        filmService.addFilm(userDetails.getUserId(), filmDTO);
        return ResponseEntity.ok(new ResponseDto("Film added successfully"));
    }

    @ApiOperation("Deleting a movie by id")
    @PreAuthorize("hasAnyAuthority('USER')")
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto> deletingFilm(
            @PathVariable(name = "id")
            @Min(value = 1L) Long id) {
        filmService.delById(id);
        return ResponseEntity.ok(new ResponseDto("Film deleted successfully "));
    }

    @ApiOperation("Get film by id")
    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<FilmDTO> getFilmById(
            @PathVariable(name = "id")
            @Min(value = 1L) Long id) {
        return ResponseEntity.ok(filmService.findById(id));
    }
}
