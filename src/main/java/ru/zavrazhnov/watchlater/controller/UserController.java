package ru.zavrazhnov.watchlater.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.zavrazhnov.watchlater.dto.SignInDTO;
import ru.zavrazhnov.watchlater.dto.TokenDTO;
import ru.zavrazhnov.watchlater.dto.UserDTO;
import ru.zavrazhnov.watchlater.models.User;
import ru.zavrazhnov.watchlater.service.SignInService;
import ru.zavrazhnov.watchlater.service.SignUpService;
import ru.zavrazhnov.watchlater.service.UserService;

import java.util.List;


@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final SignUpService signUpService;
    private final SignInService signInService;

    @ApiOperation("Registration new user by log & pass")
    @PostMapping("/signup")
    public ResponseEntity<UserDTO> signUp(@RequestBody SignInDTO sign) {
        return ResponseEntity.ok(signUpService.signUp(sign));
    }

    @ApiOperation("Authenticate user, receiving JWT")
    @PostMapping("/signin")
    public @ResponseBody
    ResponseEntity<TokenDTO> signIn(@RequestBody SignInDTO sign) {
        return ResponseEntity.ok(signInService.signIn(sign));
    }

    @ApiOperation("Getting all users, for ADMIN only")
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }


}
