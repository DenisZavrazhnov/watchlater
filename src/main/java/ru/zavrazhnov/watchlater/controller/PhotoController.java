package ru.zavrazhnov.watchlater.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.zavrazhnov.watchlater.config.jwt.UserDetailsImpl;
import ru.zavrazhnov.watchlater.dto.ResponseDto;
import ru.zavrazhnov.watchlater.models.ProfilePhoto;
import ru.zavrazhnov.watchlater.service.ProfilePhotoService;

import java.io.IOException;

@RestController
@RequestMapping("/image")
@RequiredArgsConstructor
public class PhotoController {
    private final ProfilePhotoService profilePhotoService;

    @ApiOperation("Add profile photo")
    @PreAuthorize("hasAuthority('USER')")
    @PostMapping()
    public ResponseEntity<ResponseDto> addPhoto(@RequestPart(name = "image") MultipartFile image) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        profilePhotoService.addPhoto(userDetails.getUserId(), image);
        return ResponseEntity.ok(new ResponseDto("Photo added successfully"));
    }

    @GetMapping("/get")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<ProfilePhoto> getPhoto(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        return ResponseEntity.ok(profilePhotoService.getPhoto(userDetails.getUserId()));
    }
}
