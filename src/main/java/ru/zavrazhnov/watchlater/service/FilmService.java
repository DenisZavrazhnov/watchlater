package ru.zavrazhnov.watchlater.service;


import ru.zavrazhnov.watchlater.dto.FilmDTO;

import java.util.List;

public interface FilmService {
    FilmDTO findById(Long id);

    void delById(long id);

    void addFilm(long userId, FilmDTO filmDTO);

    List<FilmDTO> getAllFilms(long userId);

}
