package ru.zavrazhnov.watchlater.service;

import ru.zavrazhnov.watchlater.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findById(Long id);

    List<User> getAllUsers();

    User save(User user);

}
