package ru.zavrazhnov.watchlater.service;

import ru.zavrazhnov.watchlater.dto.SignInDTO;
import ru.zavrazhnov.watchlater.dto.UserDTO;

public interface SignUpService {
    UserDTO signUp(SignInDTO sign);
}
