package ru.zavrazhnov.watchlater.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zavrazhnov.watchlater.dto.FilmDTO;
import ru.zavrazhnov.watchlater.models.Film;
import ru.zavrazhnov.watchlater.models.User;
import ru.zavrazhnov.watchlater.repository.FilmRepository;
import ru.zavrazhnov.watchlater.repository.UserRepository;
import ru.zavrazhnov.watchlater.service.FilmService;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FilmServiceImpl implements FilmService {
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    @Override
    public FilmDTO findById(Long id) {
        Optional<Film> film = filmRepository.findById(id);
        if (!film.isPresent()) {
            throw new IllegalArgumentException("Film with id= " + id + " not found");
        }
        return FilmDTO.fromFilm(film.get());

    }

    @Override
    public void delById(long id) {
        filmRepository.deleteById(id);
    }

    @Override
    public void addFilm(long userId, FilmDTO filmDTO) {
        if (filmDTO == null) {
            throw new IllegalArgumentException("Film cannot be null");
        }
        if (filmDTO.getTimestamp() == null) {
            filmDTO.setTimestamp(Instant.now().getEpochSecond());
        }
        Film film = filmDTO.fromDtoToFilm(filmDTO);
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new IllegalArgumentException("User with id= " + userId + " not found");
        }
        List<Film> userFilms = user.get().getFilms();
        userFilms.add(film);
        user.get().setFilms(userFilms);
        userRepository.save(user.get());
    }

    @Override
    public List<FilmDTO> getAllFilms(long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new IllegalArgumentException("User with id= " + userId + " not found");
        }
        List<Film> films = user.get().getFilms();
        return dtoFromFilm(films);
    }

    private List<FilmDTO> dtoFromFilm(List<Film> films) {
        List<FilmDTO> filmDTOList = new ArrayList<>();
        for (Film film : films) {
            filmDTOList.add(FilmDTO.builder()
                    .id(film.getId())
                    .title(film.getTitle())
                    .timestamp(film.getTimestamp())
                    .build());
        }
        return filmDTOList;
    }
}
