package ru.zavrazhnov.watchlater.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.zavrazhnov.watchlater.dto.SignInDTO;
import ru.zavrazhnov.watchlater.dto.UserDTO;
import ru.zavrazhnov.watchlater.models.Role;
import ru.zavrazhnov.watchlater.models.State;
import ru.zavrazhnov.watchlater.models.User;
import ru.zavrazhnov.watchlater.service.SignUpService;
import ru.zavrazhnov.watchlater.service.UserService;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    @Override
    public UserDTO signUp(SignInDTO sign) {
        userService.getAllUsers().forEach(user -> {
            if (user.getName().equalsIgnoreCase(sign.getName())) {
                throw new IllegalArgumentException("User with name = " + sign.getName() + " already exist ");
            }
        });
        User user = User.builder()
                .name(sign.getName())
                .password(passwordEncoder.encode(sign.getPassword()))
                .role(Role.USER)
                .state(State.CONFIRMED)
                .build();
        userService.save(user);

        return UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .build();
    }
}
