package ru.zavrazhnov.watchlater.service.impl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.zavrazhnov.watchlater.dto.SignInDTO;
import ru.zavrazhnov.watchlater.dto.TokenDTO;
import ru.zavrazhnov.watchlater.models.User;
import ru.zavrazhnov.watchlater.repository.UserRepository;
import ru.zavrazhnov.watchlater.service.SignInService;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SignInServiceImpl implements SignInService {

    private final UserRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expirationDateInMs}")
    private String jwtExpirationInMs;

    @Override
    public TokenDTO signIn(SignInDTO sign) {
        Optional<User> user = usersRepository.findUserByName(sign.getName());
        if (!user.isPresent()) {
            throw new IllegalArgumentException("User not found");
        }
        if (!passwordEncoder.matches(sign.getPassword(), user.get().getPassword())) {
            throw new IllegalArgumentException("Wrong password");
        }
        String token = Jwts.builder()
                .setSubject(user.get().getId().toString()) // id пользователя
                .claim("name", user.get().getName()) // имя
                .claim("role", user.get().getRole().name()) // роль
                .signWith(SignatureAlgorithm.HS256, secret)// подписываем его с нашим secret
                .setExpiration(new Date(System.currentTimeMillis() + Integer.parseInt(jwtExpirationInMs)))//устанавливаем срок действия
                .compact(); // преобразовали в строку
        return new TokenDTO(token);
    }


    public boolean isValid(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        Date expirationDate = claims.getExpiration();
        Date localDate = new Date(System.currentTimeMillis());
        return expirationDate.after(localDate);
    }
}
