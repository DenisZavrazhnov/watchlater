package ru.zavrazhnov.watchlater.service.impl;

import lombok.RequiredArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.zavrazhnov.watchlater.models.ProfilePhoto;
import ru.zavrazhnov.watchlater.models.User;
import ru.zavrazhnov.watchlater.repository.ProfilePhotoRepository;
import ru.zavrazhnov.watchlater.service.ProfilePhotoService;
import ru.zavrazhnov.watchlater.service.UserService;

import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProfilePhotoServiceImpl implements ProfilePhotoService {
    private final UserService userService;
    private final ProfilePhotoRepository photoRepo;

    @Override
    public void addPhoto(long userId, MultipartFile file) throws IOException {
        Optional<User> user = userService.findById(userId);
        if (!user.isPresent()) {
            throw new IllegalArgumentException("User with id= " + userId + " not found");
        }
        ProfilePhoto photo = ProfilePhoto.builder()
                .title((user.get().getName()) + "_" + userId)
                .build();
        photo.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
        photo = photoRepo.insert(photo);
        user.get().setPhotoId(photo.getId());
        userService.save(user.get());
    }

    public ProfilePhoto getPhoto(long userId) {
        Optional<User> user = userService.findById(userId);
        if (user.isEmpty()){
            throw new IllegalArgumentException("user not found");
        }
        Optional<ProfilePhoto> photo = photoRepo.findById(user.get().getPhotoId());
        if (photo.isEmpty()) {
            throw new IllegalArgumentException("Photo with id= " + user.get().getPhotoId() + " not found");
        }
        return photo.get();
    }
}
