package ru.zavrazhnov.watchlater.service;

import ru.zavrazhnov.watchlater.dto.SignInDTO;
import ru.zavrazhnov.watchlater.dto.TokenDTO;

public interface SignInService {
    TokenDTO signIn(SignInDTO sign);

    boolean isValid(String token);
}
