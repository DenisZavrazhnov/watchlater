package ru.zavrazhnov.watchlater.service;

import org.springframework.web.multipart.MultipartFile;
import ru.zavrazhnov.watchlater.models.ProfilePhoto;

import java.io.IOException;

public interface ProfilePhotoService {
    void addPhoto(long userId, MultipartFile file) throws IOException;

    ProfilePhoto getPhoto(long userId);
}
