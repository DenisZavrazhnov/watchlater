package ru.zavrazhnov.watchlater.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.zavrazhnov.watchlater.dto.ResponseDto;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseDto handleIllegalArgumentException(IllegalArgumentException e) {
        return new ResponseDto(e.getMessage());
    }
}
