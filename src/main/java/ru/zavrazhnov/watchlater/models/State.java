package ru.zavrazhnov.watchlater.models;

public enum State {
    CONFIRMED, NOT_CONFIRMED
}
