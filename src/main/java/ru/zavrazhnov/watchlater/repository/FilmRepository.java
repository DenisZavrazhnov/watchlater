package ru.zavrazhnov.watchlater.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.zavrazhnov.watchlater.models.Film;

@Repository
public interface FilmRepository extends JpaRepository<Film,Long> {
}
