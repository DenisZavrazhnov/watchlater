package ru.zavrazhnov.watchlater.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.zavrazhnov.watchlater.models.ProfilePhoto;

@Repository
public interface ProfilePhotoRepository extends MongoRepository<ProfilePhoto,String> {
}
