package ru.zavrazhnov.watchlater;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.zavrazhnov.watchlater.dto.FilmDTO;
import ru.zavrazhnov.watchlater.dto.SignInDTO;
import ru.zavrazhnov.watchlater.dto.TokenDTO;
import ru.zavrazhnov.watchlater.service.FilmService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class WatchlaterApplicationTests {

    private String token;
    @Autowired
    private MockMvc mockMvc;
    private TokenDTO tokenDTO;
    private FilmDTO[] filmDTO;
    private Optional<FilmDTO> filmResponse;
    private ObjectMapper objectMapper;
    private ObjectWriter objectWriter;
    private FilmService filmService;
    private final SignInDTO sign = new SignInDTO("test", "test");

    @BeforeEach
    public void signIn() throws Exception {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String req = objectWriter.writeValueAsString(sign);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/signin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(req)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        token = result.getResponse().getContentAsString();
        tokenDTO = objectMapper.readValue(token, TokenDTO.class);
    }

    @Test
    public void addFilm() throws Exception {
        FilmDTO filmDTO = FilmDTO.builder()
                .title("TestFilm")
                .build();
        objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        String req = objectWriter.writeValueAsString(filmDTO);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/film")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", tokenDTO.getToken())
                        .content(req).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void getFilm() throws Exception {
        objectMapper = new ObjectMapper();
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/film")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", tokenDTO.getToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        filmDTO = objectMapper.readValue(result.getResponse().getContentAsString(), FilmDTO[].class);
        List<FilmDTO> filmArray = Arrays.asList(filmDTO);
        filmResponse = filmArray.stream().filter(filmDTO1 -> "TestFilm".equals(filmDTO1.getTitle())).findFirst();
        String id = Long.toString(filmResponse.get().getId());
        filmService.delById(filmResponse.get().getId());
        assertTrue(filmResponse.isPresent());

    }

//    @Test
//    public void deletFilm(String id) throws Exception {
//        mockMvc.perform(delete("/films/" + id)
//        .header("Authorization", tokenDTO.getToken())
//        ).andExpect(status().isOk());
//    }


}
